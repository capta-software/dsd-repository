package com.springboot.microservices.cconversion;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableFeignClients( "com.springboot.microservices.cconversion" )
@EnableDiscoveryClient
public class CconversionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CconversionServiceApplication.class, args);
	}

}
