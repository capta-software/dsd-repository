package blockchain.main;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;


public class Block {
	
	
	private long nounce = 777L;
	private Object valueData = null;
	private String previouslyHash = null;
	private String hash;	//WITHOUT previouslyHash
	
	
	private Block() {
		super();
	}
	public Block( Object valueDate, String previouslyHash ) {
		this();
		
		this.previouslyHash = previouslyHash;
		this.valueData = valueDate;
		
		this.hash = calculateHash();
	}
	
	public Object getValueData() {
		return valueData;
	}
	public void setValueData(Object valueData) {
		this.valueData = valueData;
	}
	
	public String getPreviouslyHash() {
		return previouslyHash;
	}
	
	public String getHash() {
		return this.hash;
	}
	
	
	private String calculateHash() {
		String objForHash = null;
		
		if( ( previouslyHash != null ) ) {
			if( valueData == null ) {
				objForHash = "NULL" + nounce;
			} else {
				objForHash = valueData.toString() + nounce;
			}
		} else {
			return null;
		}
		
		return Base64.encodeBase64String( new DigestUtils("SHA-256").digestAsHex( Base64.encodeBase64String( objForHash.getBytes() ) ).getBytes() );
	}
	
	
}
