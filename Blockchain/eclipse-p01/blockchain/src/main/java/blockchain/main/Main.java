package blockchain.main;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;


public class Main {
	
	
	public static void main( String[] args ) {
		String myName = "JOSE SANCHEZ SILVA";
		
		String myNameB64 = Base64.encodeBase64String( myName.getBytes() );
		String myNameB64S256 = new DigestUtils("SHA-256").digestAsHex( myNameB64 );
		System.out.println( new String( "Base64: " + myNameB64 ) );
		System.out.println( new String( "Length: " + myNameB64.length() ) );
		System.out.println( new String( "Hash: " + myNameB64S256 ) );
		System.out.println( new String( "Length: " + myNameB64S256.length() ) );
		
		
		
	}
	
}
