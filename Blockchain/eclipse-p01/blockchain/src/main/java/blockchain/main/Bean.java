package blockchain.main;


public class Bean {
	
	
	private Bean() {
		super();
	}
	public Bean( String firstName, String lastName ) {
		this();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
	private String firstName;
	private String lastName;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String toString() {
		return ( "START: " + this.firstName + "||" + lastName + ". END" );
	}
	
}
