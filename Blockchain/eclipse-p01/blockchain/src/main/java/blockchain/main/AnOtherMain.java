package blockchain.main;


public class AnOtherMain {
	
	
	public static void main( String[] args ) {
		
		
		//Blockchain
		Blockchain blockc = new Blockchain();
		
		//Emulate first system; with message to blockchain
		Bean bean01 = new Bean( "I", "M" );
		Block objB = new Block( bean01, "*" );
		blockc.getBlockchain().add( objB );
		
		//MORE CHANGES
		
		//Emulate a second system without report to Blockchain
	}
	
}
